import firebase from 'firebase/app';
export const firebaseConfig = {
  apiKey: "AIzaSyD8aYSWzy6YXhsbw0yKKbpOMawjqd7IazA",
  authDomain: "diary-eef20.firebaseapp.com",
  databaseURL: "https://diary-eef20.firebaseio.com",
  projectId: "diary-eef20",
  storageBucket: "diary-eef20.appspot.com",
  messagingSenderId: "366391246340",
  appId: "1:366391246340:web:b7024ce35045b53345ff51",
  measurementId: "G-CDS6PN43N1"
};
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const fbAuthProvider = new firebase.auth.FacebookAuthProvider();
