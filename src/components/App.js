import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import {getNotes,saveNote, deleteNote} from '../actions/notesAction';
import NoteCard from './NoteCard';
import {getUser} from '../actions/userAction';
import _ from 'lodash';


class App extends Component{

  constructor(props){
    super(props);
    this.state = {
      title: '',
      body: '',
      notes: this.props.notes
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderNotes = this.renderNotes.bind(this);
    this.deleteNote = this.deleteNote.bind(this);
  }


  handleChange(e){
    e.preventDefault()
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e){
    e.preventDefault()
    this.props.saveNote({title: this.state.title, body: this.state.body, uid: this.props.user.uid});
    this.props.getNotes();
    this.setState({
      title: '',
      body: '',
    })
  }

  deleteNote(id){
    this.props.deleteNote(id)
    this.props.getNotes()
  }

  renderNotes() {
    return _.map(this.props.notes, (note, key) => {
      return (
        <div>
          {note.uid === this.props.user.uid && (
            <NoteCard key={key}>
              <Link to={`/${key}`}>
                <h2>{note.title}</h2>
              </Link>
              <p>{note.body}</p>

              <div>
                <button className="btn btn-danger btn-xs" onClick={() => this.deleteNote(key)}>Delete</button>
                <button className="btn btn-info btn-xs pull-right"><Link to={`/${key}/edit`}>Update</Link></button>
              </div>

            </NoteCard>
          )}
        </div>
      );
    });
  }

  render(){
    return (
      <div className="container-fluid">
        <br/><br/>
        <div className="row">
          <div className="col-sm-2 text-center">
            <img src={this.props.user.photoURL} height="100px" className="img img-responsive circle" style={{ padding: '20px' }} />
            <h4 className="username">Welcome back <br/> <b className="text-danger">{this.props.user.displayName}</b></h4>
          </div>
          <div className="col-sm-10">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input onChange={this.handleChange} value={this.state.title} type='text' name='title' className='form-control no-border' placeholder='Title...' required />
              </div>
              <div className="form-group">
                <textarea onChange={this.handleChange} value={this.state.body} type='text' name='body' className='form-control no-border' placeholder='Write a description here...' required />
              </div>
              <div className="form-group">
                <button className="btn btn-primary col-sm-12" >Save</button>
              </div>
            </form><br/><br/><br/>
            {this.renderNotes()}
          </div>
        </div>
      </div>
    );
  }
}


function mapStateToProps(state, ownProps){
  return{
    notes: state.notes,
    user: state.user
  }
}

export default connect(mapStateToProps, {getNotes,saveNote,deleteNote,getUser})(App);
