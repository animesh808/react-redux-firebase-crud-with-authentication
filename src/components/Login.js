import React, {Component} from 'react';
import {connect} from 'react-redux';
import {googleLogin,fbLogin} from '../actions/userAction';

class Login extends Component {
  UNSAFE_componentWillMount(){
    if(this.props.user !== null){
      this.props.history.push('/');
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.user !== null){
      nextProps.history.push('/');
    }
  }

  render(){
    return(
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12 jumbotron" style={{backgroundColor: "#8fbc8f", marginTop: '-20px'}}>
            <h1>DIARY | {new Date().getFullYear()}</h1>
            <h2>Login with your favourite <b>Social Network</b> & start writing.</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <button className="btn btn-danger col-sm-6" onClick={this.props.googleLogin}>Login with Google</button>
            <button className="btn btn-primary col-sm-6" onClick={this.props.fbLogin}>Login with Facebook</button>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, onwProps) {
    return {
      user: state.user
    };
}

export default connect(mapStateToProps, {googleLogin, fbLogin})(Login);
