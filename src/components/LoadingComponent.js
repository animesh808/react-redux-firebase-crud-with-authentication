import React from 'react';
import '../styles/loading.css';

const LoadingComponent = () => {
  return (
    <div className="flex-center position-ref full-height">
      <div className="title m-b-md">Loading...</div>
    </div>
  )
}

export default LoadingComponent;
