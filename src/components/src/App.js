import React from 'react';
import logo from './logo.svg';
import './App.css';
import Authentication from './Authentication'
var firebase = require('firebase')
var firebaseConfig = {
    apiKey: "AIzaSyCYdo4LtVv1KBLzXwyV5nGT3ifg-qzkZpA",
    authDomain: "usurvey-621ba.firebaseapp.com",
    databaseURL: "https://usurvey-621ba.firebaseio.com",
    projectId: "usurvey-621ba",
    storageBucket: "usurvey-621ba.appspot.com",
    messagingSenderId: "149444561161",
    appId: "1:149444561161:web:a29bc3c7b0770ef7322bd8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <Authentication />
    </div>
  );
}

export default App;
