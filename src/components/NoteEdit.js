import React, {Component} from 'react';
import { connect } from 'react-redux';
import {getNotes,updateNote} from '../actions/notesAction';



class NoteEdit extends Component{

  constructor(props){
    super(props);
    this.state = {
      title: this.props.note.title,
      body: this.props.note.body,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(e){
    e.preventDefault()
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e){
    e.preventDefault()
    this.props.updateNote(this.props.match.params.id,{title: this.state.title, body: this.state.body, uid: this.props.uid});
    this.setState({title: '', body: ''})
    this.props.history.push('/')
  }

  render(){
    return (
      <div className="container">
        <br/><br/>
        <div className="row">
          <div className="col-sm-8 col-sm-offset-2">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input onChange={this.handleChange} value={this.state.title} type='text' name='title' className='form-control no-border' placeholder='Title...' required />
              </div>
              <div className="form-group">
                <textarea onChange={this.handleChange} value={this.state.body} type='text' name='body' className='form-control no-border' placeholder='Write a description here...' required ></textarea>
              </div>
              <div className="form-group">
                <button className="btn btn-primary col-sm-12" >Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    note: state.notes[ownProps.match.params.id],
    uid: state.user.uid
  };
}

export default connect(mapStateToProps,{getNotes,updateNote})(NoteEdit);
