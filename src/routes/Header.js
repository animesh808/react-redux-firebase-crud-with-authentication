import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {getUser,logout} from '../actions/userAction';

class Header extends Component {
  render(){
    return(
      <div>
        <nav className="navbar navbar-inverse">
          <div className="container">
            <div className="navbar-header">
              <Link className="navbar-brand" to="#">React Diary</Link>
            </div>
            <ul className="nav navbar-nav">
              {this.props.user !== null && <li className="active"><Link to="/">Home</Link></li>}

            </ul>
            <ul className="nav navbar-nav navbar-right">
              {
                this.props.user === null ?
                  <li><Link to="/login"><span className="glyphicon glyphicon-log-in"></span> Login</Link></li>
                  :
                  <li><Link to="/logout" onClick={() => this.props.logout()}><span className="glyphicon glyphicon-log-in"></span> Logout</Link></li>
                }

            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps){
  return {
    user: state.user
  }
}
export default connect(mapStateToProps, {getUser,logout})(Header);
