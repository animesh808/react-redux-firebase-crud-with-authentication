import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import Header from './routes/Header';
// import {firebaseConfig} from './firebase';
import * as serviceWorker from './serviceWorker';
// redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers';
import Login from './components/Login';
import NoteDetail from './components/NoteDetail';
import NoteEdit from './components/NoteEdit';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Loading from './components/Loading';
import AuthenticatedComponent from './components/AuthenticatedComponent';
import './styles/index.css';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));



ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Loading>
        <div>
          <Header/>
          <Switch>
            <Route path="/login" component={Login} exact={true} />
            <Redirect from="/logout" to='/login' />
            <AuthenticatedComponent>
              <Route path="/:id/edit" component={NoteEdit} exact={true} />
              <Route path="/:id" component={NoteDetail} exact={true} />
              <Route path="/" component={App} exact={true} />
            </AuthenticatedComponent>
          </Switch>
        </div>
      </Loading>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
