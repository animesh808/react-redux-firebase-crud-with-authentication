import {GET_NOTES,NOTES_STATUS} from '../actionTypes';
import { v4 as uuidv4 } from 'uuid';
import firebase from 'firebase';


export function getNotes(){
  return dispatch => {
    //as soon as this function fires show loading true
    dispatch({
      type: NOTES_STATUS,
      payload: true
    });
    firebase.database().ref('notes/').once('value').then(function(snapshot) {
      dispatch({
        type: GET_NOTES,
        payload: snapshot.val()
      });
      //show loadin false when notes are received
      dispatch({
        type: NOTES_STATUS,
        payload: false
      });
      //
    },
    () => { //wait until something changes & try again
      dispatch({
        type: NOTES_STATUS,
        payload: -1
      });
    });
  }
}

export function saveNote(note){
  return dispatch => {
    firebase.database().ref('notes/'+uuidv4()).set(note);
  }
}

export function deleteNote(id){
  return dispatch => {
    let noteRef = firebase.database().ref('notes/'+id);
    noteRef.remove()
  }
}

export function updateNote(id,note){
  return dispatch => {
    firebase.database().ref('notes/'+id).update(note);
  }
}

export function saveComment(noteId,comment){
  return dispatch => {
    const noteRef = firebase.database().ref('notes/'+noteId)
    noteRef.child('comments/'+uuidv4()).set(comment)
  }
}
